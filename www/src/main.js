$(function() {
	init();
	text_resize();

	 $('.open_popup').on('click', function(){
		popupOpen(this);
	});

	$('#chat').on('click', function(){
		if($(".chat .content").is(':hidden')){
			chatOpen()
		}else{
			chatClose()
		}
	});

	$('.chat_user #user_nav').on('click', function(){

		if($('#' + $(this).attr('navItem')).is(':hidden')){
			$('.chat_user_nav').each(function() {
				$(this).slideUp(100);
			});

			$('.chat_user_right_inner').each(function() {
				if($(this).hasClass('chat_user_right_clicked')){
					$(this).removeClass('chat_user_right_clicked');
				}
			});
			
			$(this).addClass('chat_user_right_clicked');
			$('#' + $(this).attr('navItem')).slideDown(100, text_resize);
		}else{
			$(this).removeClass('chat_user_right_clicked');
			$('#' + $(this).attr('navItem')).slideUp(100);
		}
	});

	$('.d_general').on('click', '.d_general_outer', function(){
		if($(this).next('.d_cards_deck').is(':hidden')){
			$(this).children('.d_general_inner').addClass('d_general_inner_clicked');
			$(this).next('.d_cards_deck').slideDown(200, text_resize);
		}else{
			$(this).next('.d_cards_deck').slideUp(200);
			$(this).children('.d_general_inner').removeClass('d_general_inner_clicked')
		}
	});

	$(document).on('click', '.friends_row_outer_item', function(){
		if($(this).parent('.friends_row_item').children('.f_txt').is(':hidden')){
			$('.f_btn').each(function() {
				$(this).hide('fast');
			});
			
			$('.f_txt').each(function() {
				$(this).slideUp(100);
			});
			
			$('.friends_row_item').each(function() {
				if($(this).hasClass('friends_row_clicked')){
					$(this).removeClass('friends_row_clicked');
				}
			});
			
			$(this).parent('.friends_row_item').addClass("friends_row_clicked");
			$(this).parent('.friends_row_item').children('.f_txt').slideDown(100, function(){
				$(this).parent('.friends_row_item').children('.f_btn').show('fast', text_resize);
			});
			
		}else{
			$(this).parent('.friends_row_item').removeClass("friends_row_clicked");
			$(this).parent('.friends_row_item').children('.f_btn').hide('fast', function(){
				$(this).parent('.friends_row_item').children('.f_txt').slideUp(100);
			});
		}
	});
	
	$('#chat_right').on('click', function(){
		if($(".chat_right .chat_right_content").is(':hidden')){
			$('.chat_left').css({
				"width": "66%"
			});
			
			$('.chat_right').css({
				"margin-right": "1%",
				"border-right": "0.2vw solid #b06618"
			});
			setTimeout(function(){
				$('.chat_right_content').show();
				text_resize();
			}, 300);
		}else{
			$('.chat_right_content').hide();
			setTimeout(function(){
				$('.chat_right').css({
					"border-right": "none",
					"margin-right": "-29%"
				});
				$('.chat_left').css({
					"width": "96%"
				});
			}, 100);
		}
	});

});

$( window ).resize(function() {
	init();
	popup_resize();
	text_resize();
});

$(window).load(function(){
	
	$(".scrolling").mCustomScrollbar({
		scrollButtons:{enable:true},
		axis:"y"
	});
	
	$(".scrolling_small").mCustomScrollbar({

		scrollbarPosition:"outside"
	});
});
	
function init(){	
	$('#main').css({
      "width": $(window).width(),
      "height": $(window).height()
    });
	
	if($(window).height() < $('.container').height()+10){

		$('.container').css({
		  "width": (1920 / 1080) * $(window).height()
		});
	}else{
		$('.container').css({
		  "width": 'initial'
		});
	}

	chatClose();
	
}

function popupOpen(el){
	EvironPouUp($(el).attr('cClass'), $(el).attr('sType'), $(el).attr('cRes'));	
}

function text_resize(){
	
	$('.generals .outer .inner .content .row .general .general_outer .general_inner .resizble_long').each(function() {
		$(this).css({'font-size': (parseInt($(this).parent().height()/100*35)) + "px", "line-height" :  $(this).parent().height()/3.5 + "px", "height" : $(this).parent().height() + "px", "width" : $(this).parent().width() + "px"});
		$(this).children("span").css({'font-size': (parseInt($(this).parent().height()/100*20)) + "px" });
	});
	
	$('.gen .outer .inner .p_general .p_general_outer .p_general_inner .resizble_long').each(function() {
		$(this).css({'font-size': (parseInt($(this).parent().height()/140*20)) + "px", "line-height" :  $(this).parent().height()/8 + "px", "width" : $(this).parent().width() + "px"});
		$(this).children("span").css({'font-size': (parseInt($(this).parent().height()/140*20)) + "px" });
	});
	
	$('.resizble').each(function() {
		$(this).css({'font-size': (parseInt($(this).height()/100*40)) + "px", "line-height" :  $(this).height() + "px" });
	});

	$('.resizble_2').each(function() {
		$(this).css({'font-size': (parseInt($(this).height()/100*60)) + "px", "line-height" :  $(this).height() + "px" });
	});
}

function scrollInit(){

	if($('.t_scroll').length){
		var textareaLineHeight=parseInt($(".t_scroll .textarea-wrapper textarea").css("line-height"));
					
		$(".t_scroll .textarea-wrapper").mCustomScrollbar({
			scrollInertia:0,
			scrollButtons:{enable:true},
			axis:"y",
			advanced:{autoScrollOnFocus:false},
			keyboard:{enable:false},
			snapAmount:textareaLineHeight
		});
		
		var textarea=$(".t_scroll .textarea-wrapper textarea"),textareaWrapper=$(".t_scroll .textarea-wrapper"),textareaClone=$(".t_scroll .textarea-wrapper .textarea-clone");
		
		textarea.bind("keyup keydown",function(e){
			var $this=$(this),textareaContent=$this.val(),clength=textareaContent.length,cursorPosition=textarea.getCursorPosition();
			textareaContent="<span>"+textareaContent.substr(0,cursorPosition)+"</span>"+textareaContent.substr(cursorPosition,textareaContent.length);
			textareaContent=textareaContent.replace(/\n/g,"<br />");
			textareaClone.html(textareaContent+"<br />");
			$this.css("height",textareaClone.height());
			var textareaCloneSpan=textareaClone.children("span"),textareaCloneSpanOffset=0,
				viewLimitBottom=(parseInt(textareaClone.css("min-height")))-textareaCloneSpanOffset,viewLimitTop=textareaCloneSpanOffset,
				viewRatio=Math.round(textareaCloneSpan.height()+textareaWrapper.find(".mCSB_container").position().top);
			if(viewRatio>viewLimitBottom || viewRatio<viewLimitTop){
				if((textareaCloneSpan.height()-textareaCloneSpanOffset)>0){
					textareaWrapper.mCustomScrollbar("scrollTo",textareaCloneSpan.height()-textareaCloneSpanOffset-textareaLineHeight);
				}else{
					textareaWrapper.mCustomScrollbar("scrollTo","top");
				}
			}
		});
		
		$.fn.getCursorPosition=function(){
			var el=$(this).get(0),pos=0;
			if("selectionStart" in el){
				pos=el.selectionStart;
			}else if("selection" in document){
				el.focus();
				var sel=document.selection.createRange(),selLength=document.selection.createRange().text.length;
				sel.moveStart("character",-el.value.length);
				pos=sel.text.length-selLength;
			}
			return pos;
		}
	}
	
	/*if($('.scrolling').length){
		$(".scrolling").mCustomScrollbar({
			advanced:{
				updateOnContentResize: true
			}
		});
	}*/
	if($('.scrolling').length){
		$(".scrolling").mCustomScrollbar({
			scrollButtons:{enable:true},
			axis:"y"
		});
	}
}

function popup_resize(){
	if($('.popup_container').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container').height()+10){
			$('.popup_container').css({
			  "width": (1626 / 898) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container').css({
			  "width": 'initial'
			});
		}
	}

	if($('.popup_container_small').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_small').height()+10){
			$('.popup_container_small').css({
			  "width": (1404 / 838) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_small').css({
			  "width": 'initial'
			});
		}
	}	
	
	if($('.popup_container_middle').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_middle').height()+10){
			$('.popup_container_middle').css({
			  "width": (1404 / 979) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_middle').css({
			  "width": 'initial'
			});
		}
	}

	if($('.popup_container_generals').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_generals').height()+10){
			$('.popup_container_generals').css({
			  "width": (751 / 642) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_generals').css({
			  "width": 'initial'
			});
		}
	}

	if($('.popup_container_achievements').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_achievements').height()+40){
			$('.popup_container_achievements').css({
			  "width": (1239 / 939) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_achievements').css({
			  "width": 'initial'
			});
		}
	}
	
	if($('.popup_container_friends').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_friends').height()+40){
			$('.popup_container_friends').css({
			  "width": (750 / 657) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_friends').css({
			  "width": 'initial'
			});
		}
	}

	if($('.popup_container_rating').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_rating').height()+40){
			$('.popup_container_rating').css({
			  "width": (750 / 724) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_rating').css({
			  "width": 'initial'
			});
		}
	}
	
	if($('.popup_container_error').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_error').height()+10){
			$('.popup_container_error').css({
			  "width": (391 / 217) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_error').css({
			  "width": 'initial'
			});
		}
	}	
	
	if($('.popup_container_exit').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_exit').height()+10){
			$('.popup_container_exit').css({
			  "width": (391 / 217) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_exit').css({
			  "width": 'initial'
			});
		}
	}	
	
	if($('.popup_container_letter').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_letter').height()+40){
			$('.popup_container_letter').css({
			  "width": (473 / 316) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_letter').css({
			  "width": 'initial'
			});
		}
	}	
	
	if($('.popup_container_messages').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_messages').height()+10){
			$('.popup_container_messages').css({
			  "width": (695 / 748) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_messages').css({
			  "width": 'initial'
			});
		}
	}	
	
	if($('.popup_container_p_done').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_p_done').height()+40){
			$('.popup_container_p_done').css({
			  "width": (474 / 519) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_p_done').css({
			  "width": 'initial'
			});
		}
	}	
	
	if($('.popup_container_m_error').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_m_error').height()+40){
			$('.popup_container_m_error').css({
			  "width": (643 / 617) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_m_error').css({
			  "width": 'initial'
			});
		}
	}

	if($('.popup_container_m_new').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_m_new').height()+40){
			$('.popup_container_m_new').css({
			  "width": (643 / 617) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_m_new').css({
			  "width": 'initial'
			});
		}
	}
	
	if($('.popup_container_m_read').length){

		if($('.over-layer-wrap-container-center').height() < $('.popup_container_m_read').height()+40){
			$('.popup_container_m_read').css({
			  "width": (643 / 617) * $('.over-layer-wrap-container-center').height()
			});
		}else{
			$('.popup_container_m_read').css({
			  "width": 'initial'
			});
		}
	}
}
	
function chatOpen(){
	$('.blocks').each(function() {
		$(this).css({'margin': "0 0.5%" });
	});
	
	$('.chat').css({
	  "margin-right": (($(window).width() - $('.container').width())/2 + $('.chat').width() - (($(window).width()/100)*3))*(-1)
	});
	$(".chat .content").css({"display" : "block"});
	$(".chat .content").css({"width" : "91%"});	
	setTimeout(function(){
		
		$('.chat').css({
		  "margin-right": (($(window).width() - $('.container').width())/2 + $('.chat').width() - (($(window).width()/100)*3.1) - $(".chat .content").width())*(-1)
		});
		text_resize();
	}, 1);
	$(".arrow_outer").removeClass( "off" ).addClass( "on" );
}	

function chatClose(){
	$('.chat').css({
	  "margin-right": (($(window).width() - $('.container').width())/2 + $('.chat').width() - (($(window).width()/100)*3))*(-1)
	});
	$(".chat .content").css({"width" : "0"});		

	setTimeout(function(){
		$(".chat .content").css({"display" : "none"});		
	}, 100);

	$('.blocks').each(function() {
		$(this).css({'margin': "0 4.6%" });
	});
	
	$(".arrow_outer").removeClass( "on" ).addClass( "off" );
}	

document.addEventListener("intel.xdk.device.ready",onDeviceReadyHideStatus,false);
function onDeviceReadyHideStatus(evt)
{
	init();
	text_resize();
	if(intel.xdk.device.platform == 'Android'){
		$('.exit').css({
		  "display": 'block',
		});
	}
    intel.xdk.device.hideStatusBar();
    intel.xdk.device.setAutoRotate(false);
    intel.xdk.device.setRotateOrientation('landscape');
}    

var Popup, __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
Popup = (function() {
    function Popup(outerClose, cls, bodyOverflow, noCloseBtn) {
      this.keyHandler = __bind(this.keyHandler, this);
      var $body;
      this.mainEl = $(this.html);
      this.bodyOverflow = bodyOverflow || false;
      this.el = this.mainEl.find('.over-layer-wrap-container-center');
      if (!noCloseBtn) {
        this.el.append('<div class="popupClose"></div>');
      }
      if (cls) {
        this.mainEl.addClass(cls);
      }
      $body = $('body');
      if (bodyOverflow) {
        $body.css({
          'overflow-y': 'hidden'
        });
      }
      $body.append(this.mainEl);
      $('.popupClose', this.mainEl).click((function(_this) {
        return function() {
          return _this.close();
        };
      })(this));
      $body.on('keyup', this.keyHandler);
      this.el.click(function(e) {});
      if (outerClose) {
        this.mainEl.click((function(_this) {
          return function(e) {
            var target;
            target = e.target || e.srcElement;
            if ($(target).parents('body').length > 0 && !$(target).parents('.over-layer-wrap-container-center').length && !$(target).hasClass('over-layer-wrap-container-center')) {
              return _this.close();
            }
          };
        })(this));
      }
    }

    Popup.prototype.keyHandler = function(e) {
      if (e.keyCode === 27) {
        return this.close();
      }
    };

    Popup.prototype.close = function(silent) {
      if (this.bodyOverflow) {
        $('body').css({
          overflow: 'auto'
        });
      }
      if (!silent) {
        this.mainEl.trigger('popupClose');
      }
      this.mainEl.remove();
      return $(document).off('keyup', this.keyHandler);
    };

    Popup.prototype.html = "<div class=\"over-layer\">\n <div class=\"over-layer-wrap\">\n  <div class=\"over-layer-wrap-container\">\n   <div class=\"over-layer-wrap-container-center\">\n   </div>\n  </div>\n </div>\n</div>";

    return Popup;

  })();
 
 window.EvironPopup = Popup;

 var EvironPouUp = function(addClass, type, res, callback){

 popup = new window.EvironPopup(true, addClass, true);
 if(type == 'url'){
    var html = $('<div>').addClass('p_content');
    popup.el.append(html);
	$('.p_content').load(res,function(data){
		popup_resize();
		text_resize();
		scrollInit();
		$('.p_content').fadeTo('fast', 1);
	});
 } else if(type == 'div'){
    var html = $('<div>').addClass('p_content');
	var div = $('#'+res);
	var divClone = div.clone()
	divClone.addClass('popup_container_'+res);
    divClone.appendTo(html);
    popup.el.append(html);
    $('.popupClose').appendTo(divClone);
	divClone.show('fast', function() {
		popup_resize();
		text_resize();
		if($('.popup_container_'+res+' .ssc').length){
			$('.popup_container_'+res+' .ssc').addClass('scrolling');
		}

		scrollInit();
		$('.p_content').fadeTo('fast', 1);
		
		$('.popup_container_'+res+' label').each(function() {
			$(this).attr('for', 'm'+$(this).attr('for'));
		});
		
		$('.popup_container_'+res+' .checkbox').each(function() {
			$(this).attr('id', 'm'+$(this).attr('id'));
		});
	});
 }
/* var title = $('<div>').addClass('title').html('<span>Сообщение</span>');
 var html = $('<div>').addClass('AlertBox');
 html.append('<div class="alertText">' + text + '</div>');
 html.append('<a class="ctrl-button alertClose" href="#">Закрыть</a>');*/

 $(html).on('click', '.alertClose', function(e){
  e.preventDefault();
  popup.close();
  if(callback)
   callback();
 });
 
 $(html).on('click', '.alertOpen', function(e){
	e.preventDefault();
	popup.close();
	popupOpen(this);
 });
}