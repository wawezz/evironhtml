EvironGame = {
    table: {width: 1920, height: 1080},
	url: 'http://localhost:4000',
	path: {
		game: 'asset/game/',
		boards: 'asset/game/boards/',
		cards: 'asset/game/cards/',
		shirts: 'asset/game/shirts/',
		avatars: 'asset/game/avatars/',
		animations: 'asset/game/animations/',
		sounds: 'asset/game/sounds/',
		generalAbility: 'asset/game/generals/ability/',
		generals: 'asset/game/generals/cards/',
		icons: 'asset/game/icons/',
		tutorial: 'asset/game/tutorial/'
	},
	
	generals: {
		1: {img: 'gen_air_maeli.png' },
		2: {img: 'gen_air_maeli.png' },
		3: {img: 'gen_air_maeli.png' },
		4: {img: 'gen_air_maeli.png' },
		5: {img: 'gen_air_maeli.png' },
		6: {img: 'gen_air_maeli.png' }
	},
	
	loading: {
		1: 'Некоторые существа обладают определенными способностями и могут влиять на ход сражения. '
	},
	
	Objects: { storage: { TimeLeft: 99 }}
};

function fullScreenClick(){
	if (this.scale.isFullScreen) {
		this.scale.stopFullScreen();
	} else {
		this.scale.startFullScreen(false);
	}
}

function backButtonClick() {
	window.location.href = 'index.html';
}