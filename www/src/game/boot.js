EvironGame.boot = function (game) {

};

EvironGame.boot.prototype = {

	init: function(){
		this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.forceOrientation(true, false);
        this.scale.setResizeCallback(this.gameResized, this);
        this.scale.updateLayout(true);
        this.scale.refresh();
	},
	preload: function(){
		this.load.atlas('card_68', EvironGame.path.animations + 'card_68.png', EvironGame.path.animations + 'card_68.json');
        this.load.atlas('backButton', EvironGame.path.animations + 'backButton.png', EvironGame.path.animations + 'backButton.json');
        this.load.atlas('fullScreen', EvironGame.path.animations + 'fullscreen.png', EvironGame.path.animations + 'fullscreen.json');
		this.load.image('loading_bg',  EvironGame.path.game + 'loading_bg.png');
		this.load.image('loading_bar',  EvironGame.path.game + 'loading_bar.png');
		this.load.image('screen',  EvironGame.path.game + 'screen.jpg');
	},
	create: function(){
		this.state.start('preload');
	}
}