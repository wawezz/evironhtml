EvironGame.preload = function (game) {

};

EvironGame.preload.prototype = {
	preload: function(){
		this.loading_bar = this.add.image(this.world.centerX-415, this.world.centerY+4, 'loading_bar');	
		this.loading_bar.anchor.setTo(0, 0.5);
		
		this.bg = this.add.image(0, 0, 'loading_bg');	
		this.screen = this.add.image(this.world.centerX-17, this.world.centerY-240, 'screen');	
		this.screen.anchor.setTo(0.5, 0.5);
	
		this.logo = this.add.sprite( this.world.centerX-14, this.world.centerY+160, 'card_68');
		this.logo.animations.add('play', Phaser.Animation.generateFrameNames('Card_68_', 0, 59, '', 4), 30, true);
        this.logo.anchor.setTo(0.5, 0.5);
        this.logo.animations.play('play');
        
		this.advice = this.add.text(this.world.centerX-17, this.world.height-200, EvironGame.loading[1],  { font: "22px Cambria", fill: '#9c6b46',align: 'left', fontWeight: 'bold', wordWrap: true, wordWrapWidth: 725});
		this.advice.anchor.setTo(0.5, 0);
      /*  this.backButton = this.add.button( this.world.centerX, this.world.height-50, 'backButton', backButtonClick, this, 'over', 'out');
        this.backButton.anchor.setTo(0.5, 1);*/
		
		this.fullScreen = this.add.button(this.world.width,0, 'fullScreen', fullScreenClick, this, 'over', 'out');
		this.fullScreen.anchor.setTo(1, 0);
		
		this.load.setPreloadSprite(this.loading_bar,0);
		
		this.load.image('framePlayer',  EvironGame.path.game + 'frame-ur.png');
		this.load.image('frameOpponent',  EvironGame.path.game + 'frame-opp.png');
		this.load.image('hpPlayer',  EvironGame.path.game + 'hp_ur.png');
		this.load.image('hpOpponent',  EvironGame.path.game + 'hp_opp.png');
		this.load.image('manaPlayer',  EvironGame.path.game + 'mp_ur.png');
		this.load.image('manaOpponent',  EvironGame.path.game + 'mp_opp.png');
		this.load.image('counter', EvironGame.path.game + 'counter.png');
		this.load.image('info', EvironGame.path.game + 'info-bg.png');
		this.load.image('cardTriggers', EvironGame.path.game + 'cardTriggers.png');
		this.load.image('dialog', EvironGame.path.game + 'dialog.png');
		this.load.image('ok', EvironGame.path.game + 'ok.png');
		this.load.image('no', EvironGame.path.game + 'no.png');
		this.load.image('yturn', EvironGame.path.game + 'yturn.png');
		this.load.image('wight-wire', EvironGame.path.game + 'wight-wire.png');
		this.load.image('robot', EvironGame.path.icons + 'robot.png');
		this.load.image('swallow', EvironGame.path.icons + 'swallow.png');
		this.load.atlas('giveUp', EvironGame.path.animations + 'giveUp.png', EvironGame.path.animations + 'giveUp.json');
		this.load.atlas('deck',  EvironGame.path.animations + 'deck.png',  EvironGame.path.animations + 'deck.json');
		this.load.atlas('turn',  EvironGame.path.animations + 'turnButton.png',  EvironGame.path.animations + 'turnButton.json');
		this.load.atlas('buttonSmall',  EvironGame.path.animations + 'buttonSmall.png',  EvironGame.path.animations + 'buttonSmall.json');
		this.load.atlas('traps',  EvironGame.path.animations + 'traps.png',  EvironGame.path.animations + 'traps.json');
	
		this.load.atlas('card_1',  EvironGame.path.animations + 'card_1.png',  EvironGame.path.animations + 'card_1.json');
		this.load.atlas('card_2',  EvironGame.path.animations + 'card_2.png',  EvironGame.path.animations + 'card_2.json');
		this.load.atlas('card_2_2',  EvironGame.path.animations + 'card_2_2.png',  EvironGame.path.animations + 'card_2_2.json');
		this.load.atlas('card_3',  EvironGame.path.animations + 'card_3.png',  EvironGame.path.animations + 'card_3.json');
		this.load.atlas('card_4',  EvironGame.path.animations + 'card_4.png',  EvironGame.path.animations + 'card_4.json');
		this.load.atlas('card_4_2',  EvironGame.path.animations + 'card_4_2.png',  EvironGame.path.animations + 'card_4_2.json');
		this.load.atlas('card_5',  EvironGame.path.animations + 'card_5.png',  EvironGame.path.animations + 'card_5.json');
		if(Phaser.Device.desktop){
			this.load.atlas('card_6',  EvironGame.path.animations + 'card_6.png',  EvironGame.path.animations + 'card_6.json');
			this.load.atlas('card_7',  EvironGame.path.animations + 'card_7.png',  EvironGame.path.animations + 'card_7.json');
		}else{
			this.load.atlas('card_6',  EvironGame.path.animations + 'card_6_min.png',  EvironGame.path.animations + 'card_6_min.json');
			this.load.atlas('card_7',  EvironGame.path.animations + 'card_7_min.png',  EvironGame.path.animations + 'card_7_min.json');
		}
		this.load.atlas('card_8_2',  EvironGame.path.animations + 'card_8_2.png',  EvironGame.path.animations + 'card_8_2.json');
		this.load.atlas('card_9',  EvironGame.path.animations + 'card_9.png',  EvironGame.path.animations + 'card_9.json');
		this.load.atlas('card_10',  EvironGame.path.animations + 'card_10.png',  EvironGame.path.animations + 'card_10.json');
		this.load.atlas('card_11',  EvironGame.path.animations + 'card_11.png',  EvironGame.path.animations + 'card_11.json');
		this.load.atlas('card_12',  EvironGame.path.animations + 'card_12.png',  EvironGame.path.animations + 'card_12.json');
		this.load.atlas('card_13',  EvironGame.path.animations + 'card_13.png',  EvironGame.path.animations + 'card_13.json');
		this.load.atlas('card_16',  EvironGame.path.animations + 'card_16.png',  EvironGame.path.animations + 'card_16.json');
		this.load.atlas('card_17',  EvironGame.path.animations + 'card_17.png',  EvironGame.path.animations + 'card_17.json');
		this.load.atlas('card_18',  EvironGame.path.animations + 'card_18.png',  EvironGame.path.animations + 'card_18.json');
		this.load.atlas('card_20',  EvironGame.path.animations + 'card_20.png',  EvironGame.path.animations + 'card_20.json');
		this.load.atlas('card_21',  EvironGame.path.animations + 'card_21.png',  EvironGame.path.animations + 'card_21.json');
		this.load.atlas('card_22',  EvironGame.path.animations + 'card_22.png',  EvironGame.path.animations + 'card_22.json');
		this.load.atlas('card_23',  EvironGame.path.animations + 'card_23.png',  EvironGame.path.animations + 'card_23.json');
		this.load.atlas('card_24',  EvironGame.path.animations + 'card_24.png',  EvironGame.path.animations + 'card_24.json');
		this.load.atlas('card_25',  EvironGame.path.animations + 'card_25.png',  EvironGame.path.animations + 'card_25.json');
		this.load.atlas('card_26',  EvironGame.path.animations + 'card_26.png',  EvironGame.path.animations + 'card_26.json');
		this.load.atlas('card_27',  EvironGame.path.animations + 'card_27.png',  EvironGame.path.animations + 'card_27.json');
		this.load.atlas('card_28',  EvironGame.path.animations + 'card_28.png',  EvironGame.path.animations + 'card_28.json');
		this.load.atlas('card_29',  EvironGame.path.animations + 'card_29.png',  EvironGame.path.animations + 'card_29.json');
		this.load.atlas('card_30',  EvironGame.path.animations + 'card_30.png',  EvironGame.path.animations + 'card_30.json');
		this.load.atlas('card_31',  EvironGame.path.animations + 'card_31.png',  EvironGame.path.animations + 'card_31.json');
		this.load.atlas('card_32',  EvironGame.path.animations + 'card_32.png',  EvironGame.path.animations + 'card_32.json');
		this.load.atlas('card_33',  EvironGame.path.animations + 'card_33.png',  EvironGame.path.animations + 'card_33.json');
		this.load.atlas('card_34',  EvironGame.path.animations + 'card_34.png',  EvironGame.path.animations + 'card_34.json');
		this.load.atlas('card_35',  EvironGame.path.animations + 'card_35.png',  EvironGame.path.animations + 'card_35.json');
		this.load.atlas('card_36',  EvironGame.path.animations + 'card_36.png',  EvironGame.path.animations + 'card_36.json');
		this.load.atlas('card_37',  EvironGame.path.animations + 'card_37.png',  EvironGame.path.animations + 'card_37.json');
		this.load.atlas('card_38',  EvironGame.path.animations + 'card_38.png',  EvironGame.path.animations + 'card_38.json');
		this.load.atlas('card_39',  EvironGame.path.animations + 'card_39.png',  EvironGame.path.animations + 'card_39.json');
		this.load.atlas('card_43',  EvironGame.path.animations + 'card_43.png',  EvironGame.path.animations + 'card_43.json');		
		this.load.atlas('card_44',  EvironGame.path.animations + 'card_44.png',  EvironGame.path.animations + 'card_44.json');
		this.load.atlas('card_45',  EvironGame.path.animations + 'card_45.png',  EvironGame.path.animations + 'card_45.json');
		this.load.atlas('card_47',  EvironGame.path.animations + 'card_47.png',  EvironGame.path.animations + 'card_47.json');
		this.load.atlas('card_48',  EvironGame.path.animations + 'card_48.png',  EvironGame.path.animations + 'card_48.json');
		this.load.atlas('card_49',  EvironGame.path.animations + 'card_49.png',  EvironGame.path.animations + 'card_49.json');
		this.load.atlas('card_50',  EvironGame.path.animations + 'card_50.png',  EvironGame.path.animations + 'card_50.json');
		this.load.atlas('card_51',  EvironGame.path.animations + 'card_51.png',  EvironGame.path.animations + 'card_51.json');
		this.load.atlas('card_52',  EvironGame.path.animations + 'card_52.png',  EvironGame.path.animations + 'card_52.json');
		this.load.atlas('card_53',  EvironGame.path.animations + 'card_53.png',  EvironGame.path.animations + 'card_53.json');
		this.load.atlas('card_55',  EvironGame.path.animations + 'card_55.png',  EvironGame.path.animations + 'card_55.json');
		this.load.atlas('card_56',  EvironGame.path.animations + 'card_56.png',  EvironGame.path.animations + 'card_56.json');
		this.load.atlas('card_56_2',  EvironGame.path.animations + 'card_56_2.png',  EvironGame.path.animations + 'card_56_2.json');
		this.load.atlas('card_58',  EvironGame.path.animations + 'card_58.png',  EvironGame.path.animations + 'card_58.json');
		this.load.atlas('card_59',  EvironGame.path.animations + 'card_59.png',  EvironGame.path.animations + 'card_59.json');
		this.load.atlas('card_64',  EvironGame.path.animations + 'card_64.png',  EvironGame.path.animations + 'card_64.json');
		this.load.atlas('card_65',  EvironGame.path.animations + 'card_65.png',  EvironGame.path.animations + 'card_65.json');
		this.load.atlas('card_66',  EvironGame.path.animations + 'card_66.png',  EvironGame.path.animations + 'card_66.json');
		this.load.atlas('card_67',  EvironGame.path.animations + 'card_67.png',  EvironGame.path.animations + 'card_67.json');
		this.load.audio('defendDefault', EvironGame.path.sounds + 'defendDefault.mp3');
		this.load.audio('s_card_1', EvironGame.path.sounds + 's_card_1.mp3');
		this.load.audio('s_card_2_2', EvironGame.path.sounds + 's_card_2_2.mp3');
		this.load.audio('s_card_3', EvironGame.path.sounds + 's_card_3.mp3');
		this.load.audio('s_card_5', EvironGame.path.sounds + 's_card_5.mp3');
		this.load.audio('s_card_6', EvironGame.path.sounds + 's_card_6.mp3');
		this.load.audio('s_card_7', EvironGame.path.sounds + 's_card_7.mp3');
		this.load.audio('s_card_8_2', EvironGame.path.sounds + 's_card_8_2.mp3');
		this.load.audio('s_card_9', EvironGame.path.sounds + 's_card_9.mp3');
		this.load.audio('s_card_10', EvironGame.path.sounds + 's_card_10.mp3');
		this.load.audio('s_card_11', EvironGame.path.sounds + 's_card_11.mp3');
		this.load.audio('s_card_12', EvironGame.path.sounds + 's_card_12.mp3');
		this.load.audio('s_card_28', EvironGame.path.sounds + 's_card_28.mp3');
		this.load.audio('s_card_29', EvironGame.path.sounds + 's_card_29.mp3');
		this.load.audio('s_card_30', EvironGame.path.sounds + 's_card_30.mp3');
		this.load.audio('s_card_31', EvironGame.path.sounds + 's_card_31.mp3');
		this.load.audio('s_card_32', EvironGame.path.sounds + 's_card_32.mp3');
		this.load.audio('s_card_33', EvironGame.path.sounds + 's_card_33.mp3');	
		this.load.audio('s_card_43', EvironGame.path.sounds + 's_card_43.mp3');
		this.load.audio('s_card_47', EvironGame.path.sounds + 's_card_47.mp3');
		this.load.audio('s_card_48', EvironGame.path.sounds + 's_card_48.mp3');
		this.load.audio('s_card_49', EvironGame.path.sounds + 's_card_49.mp3');
		this.load.audio('s_card_51', EvironGame.path.sounds + 's_card_51.mp3');
		this.load.audio('s_card_52', EvironGame.path.sounds + 's_card_52.mp3');
		this.load.audio('s_card_53', EvironGame.path.sounds + 's_card_53.mp3');
		this.load.audio('s_card_55', EvironGame.path.sounds + 's_card_55.mp3');
		this.load.audio('s_card_59', EvironGame.path.sounds + 's_card_59.mp3');
		this.load.audio('s_card_64', EvironGame.path.sounds + 's_card_64.mp3');
		this.load.audio('s_card_65', EvironGame.path.sounds + 's_card_65.mp3');
		this.load.audio('s_card_66', EvironGame.path.sounds + 's_card_66.mp3');
		this.load.audio('s_card_67', EvironGame.path.sounds + 's_card_67.mp3');
		this.load.audio('s_music', EvironGame.path.sounds + 's_music_'+ (Math.floor(Math.random() * 3) + 1) + '.mp3');
	},
	create: function(){
		var player   = new EvironGame.Objects.Player();
		var opponent = new EvironGame.Objects.Player();

		EvironGame.Objects.storage.Player   = player;
		EvironGame.Objects.storage.Opponent = opponent;

		EvironGame.Objects.storage.Players = {};

		player.opponent = opponent;
		opponent.opponent = player;

		opponent.top = true;
		EvironGame.ConnectToServer();
	}
}