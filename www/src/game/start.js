EvironGame.start = function () {
	if (EvironGame.Objects.storage.gameIsStart) return;
    EvironGame.Objects.storage.gameIsStart = true;  
};

EvironGame.start.prototype = 
{
	preload: function ()
	{	
		table = this.add.image(0, 0, 'table');	
		table.inputEnabled = true;
		//table.events.onInputUp.add(click);	
		table.input.priorityID = 0;
		
		EvironGame.Objects.PlayerZones();
		//EvironGame.Objects.GameZones();
		
		/*EVIRON.Game.Attackers = EVIRON.Scene.add.group();
		
		EVIRON.card_43 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_43');
		EVIRON.card_43.animations.add('play', Phaser.Animation.generateFrameNames('Card_43_', 0, 76, '', 4), 30, false);
		EVIRON.card_43.anchor.setTo(0.5, 0.5);
		EVIRON.card_43.alpha = 0;

		EVIRON.card_47 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_47');
		EVIRON.card_47.animations.add('play', Phaser.Animation.generateFrameNames('Card_47_', 0, 31, '', 4), 30, false);
		EVIRON.card_47.anchor.setTo(0.5, 0.5);
		EVIRON.card_47.alpha = 0;

		EVIRON.card_64 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_64');
		EVIRON.card_64.animations.add('play', Phaser.Animation.generateFrameNames('Card_64_', 0, 40, '', 4), 30, false);
		EVIRON.card_64.anchor.setTo(0.5, 0.5);
		EVIRON.card_64.alpha = 0;

		EVIRON.card_65 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_65');
		EVIRON.card_65.animations.add('play', Phaser.Animation.generateFrameNames('Card_65_', 0, 59, '', 4), 30, false);
		EVIRON.card_65.anchor.setTo(0.5, 0.5);
		EVIRON.card_65.alpha = 0;
	
		EVIRON.card_66 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_66');
		EVIRON.card_66.animations.add('play', Phaser.Animation.generateFrameNames('Card_66_', 0, 39, '', 4), 30, false);
		EVIRON.card_66.anchor.setTo(0.5, 0.5);
		EVIRON.card_66.alpha = 0;
	
		EVIRON.card_52 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_52');
		EVIRON.card_52.animations.add('play', Phaser.Animation.generateFrameNames('Card_52_', 0, 77, '', 4), 30, false);
		EVIRON.card_52.anchor.setTo(0.5, 0.5);
		EVIRON.card_52.alpha = 0;
		
		EVIRON.card_51 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_51');
		EVIRON.card_51.animations.add('play', Phaser.Animation.generateFrameNames('Card_51_', 0, 78, '', 4), 30, false);
		EVIRON.card_51.anchor.setTo(0.5, 0.5);
		EVIRON.card_51.alpha = 0;
			
		EVIRON.card_67 = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_67');
		EVIRON.card_67.animations.add('play', Phaser.Animation.generateFrameNames('Card_67_', 0, 60, '', 4), 30, false);
		EVIRON.card_67.anchor.setTo(0.5, 0.5);
		EVIRON.card_67.alpha = 0;

		EVIRON.yturn = EVIRON.Scene.add.image(EVIRON.Scene.world.centerX, EVIRON.Scene.world.centerY, 'yturn');
		EVIRON.yturn.alpha = 0;
		EVIRON.yturn.anchor.setTo(0.5, 0.5); 
		
		EVIRON.graphics = EVIRON.Scene.add.graphics(0, 0);
		EVIRON.graphics.beginFill(0x000000);
		EVIRON.graphics.drawRect(0, 0, EVIRON.TABLE_WIDTH, EVIRON.TABLE_HEIGHT);
		EVIRON.graphics.alpha = 0;
		EVIRON.graphics.endFill();
		EVIRON.avatar = EVIRON.Scene.add.image(EVIRON.Scene.world.centerX, EVIRON.Scene.world.centerY+33, 'avatarPlayer');
		EVIRON.avatar.alpha = 0;
		EVIRON.avatar.scale.setTo(0.83, 0.83);
		EVIRON.avatar.anchor.setTo(0.5, 0.5); 
		EVIRON.lose = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_6');
		EVIRON.lose.animations.add('play', Phaser.Animation.generateFrameNames('Card_6_', 0, 124, '', 4), 30, false);
		EVIRON.lose.anchor.setTo(0.5, 0.5);
		EVIRON.win = EVIRON.Scene.add.sprite(EVIRON.Scene.world.centerX,EVIRON.Scene.world.centerY, 'card_7');
		EVIRON.win.animations.add('play', Phaser.Animation.generateFrameNames('Card_7_', 0, 100, '', 4), 30, false);
		EVIRON.win.anchor.setTo(0.5, 0.5);
		EVIRON.Sounds = {};
		EVIRON.defendDefault = EVIRON.Scene.add.audio('defendDefault');
		EVIRON.Sounds.s_card_1   = EVIRON.Scene.add.audio('s_card_1');
		EVIRON.Sounds.s_card_2_2 = EVIRON.Scene.add.audio('s_card_2_2');
		EVIRON.Sounds.s_card_3   = EVIRON.Scene.add.audio('s_card_3');
		EVIRON.Sounds.s_card_5   = EVIRON.Scene.add.audio('s_card_5');
		EVIRON.Sounds.s_card_6   = EVIRON.Scene.add.audio('s_card_6');
		EVIRON.Sounds.s_card_7   = EVIRON.Scene.add.audio('s_card_7');
		EVIRON.Sounds.s_card_8_2 = EVIRON.Scene.add.audio('s_card_8_2');
		EVIRON.Sounds.s_card_9   = EVIRON.Scene.add.audio('s_card_9');
		EVIRON.Sounds.s_card_10  = EVIRON.Scene.add.audio('s_card_10');
		EVIRON.Sounds.s_card_11  = EVIRON.Scene.add.audio('s_card_11');
		EVIRON.Sounds.s_card_12  = EVIRON.Scene.add.audio('s_card_12');
		EVIRON.Sounds.s_card_28  = EVIRON.Scene.add.audio('s_card_28');
		EVIRON.Sounds.s_card_29  = EVIRON.Scene.add.audio('s_card_29');
		EVIRON.Sounds.s_card_30  = EVIRON.Scene.add.audio('s_card_30');		
		EVIRON.Sounds.s_card_31  = EVIRON.Scene.add.audio('s_card_31');
		EVIRON.Sounds.s_card_32  = EVIRON.Scene.add.audio('s_card_32');
		EVIRON.Sounds.s_card_33  = EVIRON.Scene.add.audio('s_card_33');
		EVIRON.Sounds.s_card_43  = EVIRON.Scene.add.audio('s_card_43');
		EVIRON.Sounds.s_card_47  = EVIRON.Scene.add.audio('s_card_47');
		EVIRON.Sounds.s_card_48  = EVIRON.Scene.add.audio('s_card_48');
		EVIRON.Sounds.s_card_49  = EVIRON.Scene.add.audio('s_card_49');
		EVIRON.Sounds.s_card_51  = EVIRON.Scene.add.audio('s_card_51');
		EVIRON.Sounds.s_card_52  = EVIRON.Scene.add.audio('s_card_52');
		EVIRON.Sounds.s_card_53  = EVIRON.Scene.add.audio('s_card_53');
		EVIRON.Sounds.s_card_55  = EVIRON.Scene.add.audio('s_card_55');
		EVIRON.Sounds.s_card_59  = EVIRON.Scene.add.audio('s_card_59');
		EVIRON.Sounds.s_card_64  = EVIRON.Scene.add.audio('s_card_64');		
		EVIRON.Sounds.s_card_65  = EVIRON.Scene.add.audio('s_card_65');
		EVIRON.Sounds.s_card_66  = EVIRON.Scene.add.audio('s_card_66');
		EVIRON.Sounds.s_card_67  = EVIRON.Scene.add.audio('s_card_67');
		EVIRON.Sounds.s_music    = EVIRON.Scene.add.audio('s_music');
//		EVIRON.Sounds.s_music_clouds.play();
		EVIRON.Sounds.s_music.loopFull(0.3)
			
		fullScreen2 = EVIRON.Scene.add.button(EVIRON.TABLE_WIDTH-12,0, 'fullScreen', fullScreenClick, this, 'over', 'out');
		if(fullScreen2 != 'undefined') {
                    fullScreen2.anchor.setTo(1, 0);
                }
		if(EVIRON.Params.tutorial){
			EVIRON.tutorialGraphics = EVIRON.Scene.add.graphics(0, 0);
			EVIRON.tutorialGraphics.beginFill(0x000000);
			EVIRON.tutorialGraphics.drawRect(0, 0, EVIRON.TABLE_WIDTH, EVIRON.TABLE_HEIGHT);
			EVIRON.tutorialGraphics.alpha = 0;	
			EVIRON.tutorialGraphics.endFill();
			EVIRON.tutorialGraphics.inputEnabled = true;
			EVIRON.tutorialGraphics.input.useHandCursor = true;
			EVIRON.tutorialGraphics.input.priorityID = 100;
			
			EVIRON.TUTORIAL_EVENTS = {
				7 : {
						0:{
							text:'Приветствуем тебя в мире Эвирона, '+EVIRON.Game.storage.Player.nick+'! \n\n Ниары рады приветствовать любого, кто готов  встать на пути зла Гримтрона. Мы постараемся обучить тебя всему, что нужно знать, вступая в своё первое в жизни сражение. Приготовься!', 
							image:'dialog0',
							textX: -120,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							secondPosition:{x:440,y:805},
							tween: true,
							hide: false,
							bg: true
						},
						1:{
							text:'Хо-хо-хо! Вы привели в Мир нового героя! \n\n Что ж, наш полководец быстро научит его хорошим манерам. Ваш защитник сбежит из Эвирона быстрее испуганного носорога.  Пусть попробует доказать свою пригодность. Готовьте его к сражению!', 
							image:'dialog1',
							textX: -335,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							secondPosition:{x:990,y:211},
							tween: true,
							hide: 'dialog1',
							bg: true
						},
						2:{
							text:'Удачи тебе, '+EVIRON.Game.storage.Player.nick+'. \n\n Мы верим в твою уникальную силу, которая поможет справиться с любыми трудностями. Благословляем твоё оружие и ждём, что твой путь в мире Эвирона начнётся с победы!', 
							image:'dialog0',
							textX: -110,
							firstPosition:{x:440,y:805},
							tween: false,
							hide: 'dialog0',
							bg: true
						},
						3:{
							text:'Не волнуйся герой! В эту битву ты не пойдёшь в одиночку, мы вместе одолеем заносчивых шиоттов и их хвалёного чемпиона. Я буду подсказывать и направлять твои первые шаги. Победа будет за нами!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:440,y:805},
							secondPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							tween: true,
							hide: false,
							bg: true							
						},
						4:{
							text:'Давай внимательно изучим возможности, которые доступны во время сражения. Начнём с самого главного: вот индикаторы жизни, которые показывают её состояние во время битвы. Тот, у кого она раньше опустится до 0, проиграл.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							tween: false,
							hide: 'dialog2',
							addImg:{
								0: { name: 'hpPlayer', position:{x:EVIRON.Scene.world.centerX-522, y:EVIRON.TABLE_HEIGHT-334}},
								1: { name: 'hpOpponent', position:{x:EVIRON.Scene.world.centerX-522, y:120}}
							},
							bg: true
						},
						5:{
							text:'Теперь стоит обратить внимание на индикаторы энергии. Каждый ход она растёт на одну единицу до тех пор, пока не достигнет значения 10. Чем больше у тебя энергии, тем больше карт можно разыграть.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							tween: false,
							hide: 'dialog2',
							addImg:{
								0: { name: 'manaPlayer', position:{x:EVIRON.Scene.world.centerX+96, y:EVIRON.TABLE_HEIGHT-333}},
								1: { name: 'manaOpponent', position:{x:EVIRON.Scene.world.centerX+96, y:120}}
							},
							bg: true
						},
						6:{
							text:'Не забывай, что я или другой полководец Стихий всегда будет поддерживать тебя в бою своими уникальными способностями. Увы, даже наши силы требуют энергии:  5 единиц стоит выход на поле боя, а использование особого таланта – 2 единицы.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							tween: false,
							hide: 'dialog2',
							addImg:{
								0: { name: 'generalPlayer', position:{x:28, y:EVIRON.TABLE_HEIGHT-234}, scale:0.32},
								1: { name: 'generalPlayerAbilityButton', position:{x:179, y:EVIRON.TABLE_HEIGHT-215}},
								2: { name: 'generalPlayerSummonButton', position:{x:179, y:EVIRON.TABLE_HEIGHT-120}}
							},
							bg: true
						},
						7:{
							text:'Теперь перейдём к колоде.  Всего в сражении у тебя будет 30 карт. Каждый ход из колоды в руку будет приходить одна из них. Постарайся использовать её наиболее разумным способом, чтобы получить преимущество на поле боя.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							tween: false,
							hide: 'dialog2',
							addImg:{
								0: { name: 'shirtPlayer', position:{x:EVIRON.TABLE_WIDTH-173, y:EVIRON.TABLE_HEIGHT-240}, scale:0.307}
							},
							bg: true
						},
						8:{
							text:'Обрати своё внимание на таймер хода и кнопку «Завершить ход». Если ты успел завершить все свои действия раньше, чем истечёт время, не стоит утомлять себя и противника долгим ожиданием.  Что ж, теперь ты готов к своему первому бою!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY},
							secondPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY-60},
							tween: true,
							hide: 'dialog2',
							addImg:{
								0: { name: 'btnTurn', position:{x:EVIRON.TABLE_WIDTH-100, y:EVIRON.Scene.world.centerY-52}}
							},
							bg: true
						},
						9:{
							text:'Начнём же битву! Твой противник явно заскучал. Первый ход за нами – не будем тратить его напрасно. Выпустим на поле боя наше первое существо - «Плотоядную саранчу». Оно как раз стоит 1 единицу энергии, нам это по карману.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX, y:EVIRON.Scene.world.centerY-60},
							tween: false,
							hide: false,
							cardHand: true,
							cardTop: true,
							click: true,
							clickId: 4,
							clickObject: 'Hand',
							bg: true,
							polygon: {1:{x:760,y:625}, 2:{x:1040,y:630}, 3:{x:1030,y:1065}, 4:{x:745,y:1057}}
						}
					},
				11 : {
					10:{
							text: EVIRON.Game.storage.Player.nick+' заверши ход!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'Turn',
							polygon: {1:{x:1347,y:500}, 2:{x:1430,y:500}, 3:{x:1430,y:580}, 4:{x:1347,y:580}}
						}
					},
				25 : {
					11:{
							text: 'Смотри, Гриалот тоже вывел существо. Давай опробуем наши магические способности и ударим его "Гудящей напастью".', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: 'dialog2',
							cardHand: true,
							click: true,
							clickId: 3,
							clickObject: 'Hand',
							wait: 0.3,
							polygon: {1:{x:644,y:627}, 2:{x:924,y:632}, 3:{x:924,y:1067}, 4:{x:639,y:1059}}
						},
					12:{
							click: true,
							clickId: 0,
							clickObject: 'DamageCardSpell',
							polygon: {1:{x:648,y:352}, 2:{x:793,y:352}, 3:{x:793,y:526}, 4:{x:648,y:526}}
						}
					},
				30 : {
					13:{
							text: 'Отлично! Оно уничтожено! Не мешкай, отдай команду атаковать самого противника!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							wait: 3.5,
							polygon: {1:{x:648,y:551}, 2:{x:793,y:551}, 3:{x:793,y:725}, 4:{x:648,y:725}}
						},
					14:{
							selfId: 0,
							click: true,
							clickObject: 'Avatar',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
					},
				34 : {
					15:{
							text: 'Отлично, '+EVIRON.Game.storage.Player.nick+' заверши ход!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'Turn',
							polygon: {1:{x:1347,y:500}, 2:{x:1430,y:500}, 3:{x:1430,y:580}, 4:{x:1347,y:580}}
						}
					},
				48 : {
					16:{
							text: 'Ой-ой-ой, он кажется испугался и вывел на поле боя Зачинщика. Отдай команду Плотоядной саранче уничтожить вражеское существо - оно мешает нам атаковать противника.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: 'dialog2',
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							polygon: {1:{x:648,y:551}, 2:{x:793,y:551}, 3:{x:793,y:725}, 4:{x:648,y:725}}
						},
						17:{
							click: true,
							clickId: 0,
							selfId: 0,
							clickObject: 'DamageCard',
							polygon: {1:{x:648,y:352}, 2:{x:793,y:352}, 3:{x:793,y:526}, 4:{x:648,y:526}}
						}
					},
				56 : {
					18:{
							text: 'Пришло время опробовать возможности моего особого умения! Используй силу Грозового разряда на противнике.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'GeneralAbility',
							wait: 2.5,
							polygon: {1:{x:185,y:870}, 2:{x:260,y:870}, 3:{x:260,y:965}, 4:{x:185,y:965}},
							arrow: {r:180, x:380, y:EVIRON.TABLE_HEIGHT-210}
						},
					19:{
							hide: 'dialog2',
							click: true,
							clickObject: 'GeneralAbilityPlay',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
					},
				61 : {
					20:{
							text: 'Отлично, '+EVIRON.Game.storage.Player.nick+' заверши ход!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'Turn',
							wait: 1,
							polygon: {1:{x:1347,y:500}, 2:{x:1430,y:500}, 3:{x:1430,y:580}, 4:{x:1347,y:580}}
						}
					},
				75 : {
					21:{
							text: 'Теперь, давай вызовем на поле боя кого-то посерьёзнее. Например, Шуструю Ласточку. Она обладает Спешкой и может атаковать, не дожидаясь следующего хода.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							cardHand: true,
							click: true,
							clickId: 2,
							clickObject: 'Hand',
							tween: false,
							hide: false,
							wait: 0.3,
							polygon: {1:{x:485,y:630}, 2:{x:765,y:627}, 3:{x:775,y:1063}, 4:{x:490,y:1065}}
						}
					},
				79 : {
					22:{
							text: 'Чтобы её удар нанёс больше урона, используем заклинание "Накал" и повысим атаку нашего существа.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							cardHand: true,
							click: true,
							clickId: 1,
							clickObject: 'Hand',
							wait: 0.3,
							polygon: {1:{x:413,y:630}, 2:{x:700,y:627}, 3:{x:710,y:1063}, 4:{x:423,y:1065}}
						},
					23:{
							click: true,
							clickId: 0,
							clickObject: 'AddCardSpell',
							polygon: {1:{x:648,y:551}, 2:{x:793,y:551}, 3:{x:793,y:725}, 4:{x:648,y:725}}
						}
					},
				84 : {
					24:{
							text: 'Посмотри сам насколько сильнее она стала – открой книжку её талантов. Похоже нам удастся сегодня посрамить полководца шиоттов.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							click: true,
							clickId: 0,
							clickObject: 'Book',
							hide: 'dialog2',
							wait: 2,
							polygon: {1:{x:700,y:700}, 2:{x:740,y:700}, 3:{x:740,y:725}, 4:{x:700,y:725}},
							arrow: {r:0, x:560, y:725}
						},
					25:{
							click: true,
							clickObject: 'BookRemove'
						},
					26:{
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							polygon: {1:{x:648,y:551}, 2:{x:793,y:551}, 3:{x:793,y:725}, 4:{x:648,y:725}}
						},
					27:{
							selfId: 0,
							click: true,
							clickObject: 'Avatar',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
					},
				88 : {
					28:{
							text: 'Отлично, '+EVIRON.Game.storage.Player.nick+' заверши ход!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'Turn',
							wait: 0.5,
							polygon: {1:{x:1347,y:500}, 2:{x:1430,y:500}, 3:{x:1430,y:580}, 4:{x:1347,y:580}}
					}
				},
				109 : {
					29:{
							text: 'Что ж, мы слишком увлеклись атакой и его Косматый медведь смог улучить момент и нанести нам удар. Но ничего, следующего раза мы не допустим  – установим ловушку «Вихрь».  А нашей Ласточке пора в атаку.',
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							cardHand: true,
							click: true,
							clickId: 0,
							clickObject: 'Hand',
							hide: 'dialog2',
							wait: 0.3,
							polygon: {1:{x:300,y:634}, 2:{x:580,y:622}, 3:{x:610,y:1060}, 4:{x:310,y:1073}}
						}
				},					
				113 : {
					30:{
							text: 'Выведите на поле боя Угольного жучка и направьте всю мощь своих войск на противника, не обращай внимания на его существ.',
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: 'dialog2',
							cardHand: true,
							click: true,
							clickId: 1,
							clickObject: 'Hand',
							polygon: {1:{x:451,y:630}, 2:{x:738,y:627}, 3:{x:748,y:1063}, 4:{x:461,y:1065}}
						},
					31:{
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							polygon: {1:{x:573,y:551}, 2:{x:718,y:551}, 3:{x:718,y:725}, 4:{x:573,y:725}}
						},
					32:{
							selfId: 0,
							click: true,
							clickObject: 'Avatar',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
				},
				120 : {
					33:{
							click: true,
							clickId: 1,
							clickObject: 'Mark',
							polygon: {1:{x:722,y:551}, 2:{x:868,y:551}, 3:{x:868,y:725}, 4:{x:722,y:725}}
						},
					34:{
							selfId: 1,
							click: true,
							clickObject: 'Avatar',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
				},
				124 : {
					35:{
							text: 'Отлично, '+EVIRON.Game.storage.Player.nick+' заверши ход!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'Turn',
							wait: 0.5,
							polygon: {1:{x:1347,y:500}, 2:{x:1430,y:500}, 3:{x:1430,y:580}, 4:{x:1347,y:580}}
						}
				},
				144 : {
					36:{
							text: 'Похоже, Гриалот решил лично расправиться с тобой. Не мешкай, выпускай меня на поле боя. Постараюсь его удержать, пока ты выпустишь в битву новых бойцов!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'GeneralSummonPlay',
							polygon: {1:{x:185,y:970}, 2:{x:260,y:970}, 3:{x:260,y:1065}, 4:{x:185,y:1065}},
							arrow: {r:180, x:380, y:EVIRON.TABLE_HEIGHT-110}
						}
				},
				148 : {
					37:{
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							polygon: {1:{x:498,y:551}, 2:{x:643,y:551}, 3:{x:643,y:725}, 4:{x:498,y:725}}
						},
					38:{
							selfId: 0,
							click: true,
							clickObject: 'Avatar',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
				},
				152 : {
					39:{
							click: true,
							clickId: 1,
							clickObject: 'Mark',
							polygon: {1:{x:648,y:551}, 2:{x:793,y:551}, 3:{x:793,y:725}, 4:{x:648,y:725}}
						},
					40:{
							selfId: 1,
							click: true,
							clickObject: 'Avatar',
							polygon: {1:{x:625,y:115}, 2:{x:815,y:115}, 3:{x:815,y:330}, 4:{x:625,y:330}}
						}
				},
				156 : {
					41:{
							text: 'Отлично, '+EVIRON.Game.storage.Player.nick+' заверши ход!', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'Turn',
							wait: 1,
							polygon: {1:{x:1347,y:500}, 2:{x:1430,y:500}, 3:{x:1430,y:580}, 4:{x:1347,y:580}}
						}
				},
				178 : {
					42:{
							text: 'Гриалот одолел меня, но у него осталось совсем немного защиты. Добьём его Грозовым разрядом.', 
							image:'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: false,
							click: true,
							clickObject: 'GeneralAbility',
							hide: 'dialog2',
							polygon: {1:{x:185,y:870}, 2:{x:260,y:870}, 3:{x:260,y:965}, 4:{x:185,y:965}},
							arrow: {r:180, x:380, y:EVIRON.TABLE_HEIGHT-210}
						},
					43:{
							click: true,
							clickId: 1,
							clickObject: 'GeneralAbilityPlay',
							polygon: {1:{x:648,y:352}, 2:{x:793,y:352}, 3:{x:793,y:526}, 4:{x:648,y:526}}
						}
				},
				185 : {
					44:{
							text: 'Прекрасно, теперь закончи этот бой. Добей Водного скакуна существами и прикончи Гриалота "Карой небесной".', 
							image: 'dialog2',
							textX: -140,
							firstPosition:{x:EVIRON.Scene.world.centerX+90, y:EVIRON.Scene.world.centerY-125},
							tween: false,
							hide: 'dialog2',
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							wait: 2.5,
							polygon: {1:{x:573,y:551}, 2:{x:718,y:551}, 3:{x:718,y:725}, 4:{x:573,y:725}}
						},
					45:{
							click: true,
							clickId: 0,
							selfId: 0,
							clickObject: 'DamageCard',
							polygon: {1:{x:573,y:352}, 2:{x:718,y:352}, 3:{x:718,y:526}, 4:{x:573,y:526}}
						}
				},
				192 : {
					46:{
							click: true,
							clickId: 0,
							clickObject: 'Mark',
							wait: 2,
							polygon: {1:{x:648,y:551}, 2:{x:793,y:551}, 3:{x:793,y:725}, 4:{x:648,y:725}}
						},
					47:{
							click: true,
							clickId: 0,
							selfId: 0,
							clickObject: 'DamageCard',
							polygon: {1:{x:573,y:352}, 2:{x:718,y:352}, 3:{x:718,y:526}, 4:{x:573,y:526}}
						}
				},
				200 : {
					48:{
							cardHand: true,
							click: true,
							clickId: 3,
							clickObject: 'Hand',
							polygon: {1:{x:644,y:627}, 2:{x:924,y:632}, 3:{x:924,y:1067}, 4:{x:639,y:1059}}
						},
					49:{
							click: true,
							clickObject: 'End'
						}
				}
			}
		};
		
		if(!EVIRON.Params.tutorial){
			EVIRON.Timer = EVIRON.Scene.time.create(false);

			EVIRON.Timer.loop(1000, EVIRON.Game.storage.Timer.update, this);
			EVIRON.Timer.start();
		}*/
	},
	create: function ()
	{
//		EvironGame.Emit('Ready');
	},
	update: function()
	{

	}
}