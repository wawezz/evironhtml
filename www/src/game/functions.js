function fullScreenClick(){
	if (EvironGame.game.scale.isFullScreen) {
		EvironGame.game.scale.stopFullScreen();
	} else {
		EvironGame.game.scale.startFullScreen(false);
	}
}
	
EvironGame.Emit = function (type, data) {
	var packet = { type: type, data: data }
	console.log('emit', packet);
	EvironGame.Socket.emit('GameEvent', packet);
}
	
	EvironGame.ConnectToServer = function () {

		if (typeof io == 'undefined') {
			return;
		}

		EvironGame.SetClientParams();

		var socket = false;
		if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1 && false) {
			socket = io.connect(EvironGame.url, {'transports': ['xhr-polling']});
		} else {
			socket = io.connect(EvironGame.url);
		}

		socket.on('connect', function () {
			socket.on('message', function (msg) {
				console.log('event message:', msg);
				if (msg.event == 'redirect') location.href = 'index.html';
			});

			socket.on('ClientParam', EvironGame.SentClientParam);
			socket.on('GameEvent', EvironGame.Objects.EventHandler);
			
		});
		
	    EvironGame.Socket = socket;
	};
	
	EvironGame.SetClientParams = function () {

		EvironGame.TrainingMode = location.href.indexOf('training=') !== -1;
		EvironGame.TutorialMode = location.href.indexOf('tutorial=') !== -1;
		EvironGame.DuelMode = location.href.indexOf('duel=') !== -1;
		EvironGame.duelOpponent = location.search.split('duel=')[1] ? location.search.split('duel=')[1] : '';

		EvironGame.Params = {
			training: EvironGame.TrainingMode,
			tutorial: EvironGame.TutorialMode,
			duel: EvironGame.DuelMode,
			duelOpponent: EvironGame.duelOpponent,
			general_id: 1,
			cards: '61,61,61,61,61,61,61,61,61,61,61',
			shirt: 'casing1.png',
			avatar: 'avatar_demon_male.png',
			nick: 'testUser'
		}

	}
	
	EvironGame.SentClientParam = function () {
		EvironGame.Socket.emit('ClientParam', EvironGame.Params);
	}
	
	EvironGame.Objects.EventHandler = function (packet, key) { 

		if (!key) key = 0;

		//if (Object.keys(packet).length <= 0) return;

		//packet[Object.keys(packet).length - 1].inline = true;

		EvironGame.Objects.EventData.push(packet);
		EvironGame.Objects.EventData.goIfCan();
	}

	EvironGame.Objects.EventData = {
		EVENT_ADDED: 0,
		EVENT_PARSE: 1,
		EVENT_FINISH: 2,

		wait: false,
		active: false,
		current: 0,
		complete: 0,

		pool: [],
		push: function (packet) {
			this.pool.push({data: packet, status: EvironGame.Objects.EventData.EVENT_ADDED});
		},
		goIfCan: function () {
			if (this.active !== false) return;

			for (var i in this.pool) {
				var data = this.pool[i];

				if (data.status != this.EVENT_ADDED) continue;

				this.pool[i].status = this.EVENT_PARSE;

				this.current  = 0;
				this.complete = 0;
				this.active   = i;

				//var lastEvent = this.pool[this.active].data[0];

				/*if (lastEvent.ctime && lastEvent.dtime) {
					console.log('Time left: ' + (lastEvent.dtime - lastEvent.ctime) + ' sec.');
					EvironGame.Objects.storage.Timer.TimeLeft = lastEvent.dtime - lastEvent.ctime;
				}*/

				this.parse();
				return;
			}

			if (!EvironGame.Objects.storage.SelectMode && EvironGame.Objects.storage.myturn)
				EvironGame.Objects.LightTargets('CreatureAvailMask');
		},
		parse: function () {

			if (this.timeoutID) {
				clearTimeout(this.timeoutID);
				this.timeoutID = false;
			}

			if (!this.active) return;

			var packet = this.pool[this.active].data;

			//for (var i = this.current; i < Object.keys(packet).length; i++) {

				var data = packet[data];

				/*if (data.cancel) {
					if (data.type == 'Attack') {
						EvironGame.EventHandlerContinue();
						continue;
					}
				}*/

				/*if (data.inline) {
					this.current = i + 1;
					this.wait = true;
				}*/

				EvironGame.Objects.ParseEvent(packet);

				/*if (data.inline && i + 1 < packet.length) {
					return;
				}*/
				this.wait = false;

			//}

			this.pool[this.active].status = this.EVENT_FINISH;

			this.lastActive = this.active;
			this.active = false;
			var self = this;
			
			EvironGame.game.time.events.add(Phaser.Timer.SECOND * 0.05, this.goIfCan, this);

		},
		iteration: function () {
			this.complete++;

			if (!this.wait || this.active === false) return;

			if (this.complete < this.current) return;

			if (this.complete > this.pool[this.active].data.length) {
				console.log('complete is more this.pool.data.length');
			}

			if (this.pool[this.active].status == this.EVENT_FINISH) {
				console.log('complete for finish eventParse');
			}

			this.wait = false;
			var self = this;

			EvironGame.game.time.events.add(Phaser.Timer.SECOND * 0.02, this.parse, this);

		}
	}
	
	EvironGame.Objects.EventHandlerContinue = function () {
		EvironGame.Objects.EventData.iteration();
	}
	
	EvironGame.Objects.Player = function () {
		this.nick = 'No Name';
		this.top = false;

		this.hp   = 30;
		
		this.mana = 0;

		this.MaxMana = 0;

		this.avatar = '';
		this.shirt = '';

		this.opponent = false;

		this.zones = {};
		this.cards = {};
		this.deck = 30;

		this.hand = [];
		this.battle = [];

		this.trap = [];

		this.isPlayer = true;
		this.isBot = false;
		this.isTutorialBot = false;
		this.GeneralSummon = false;
		this.GeneralAbility = false;
	}

	EvironGame.Objects.Player.prototype = {
		constructor: EvironGame.Objects.Player,
		setInfo: function (data) {

			this.id = data.id;

			this.nick = data.nick;
			this.isBot = data.isBot;
			this.isTutorialBot = data.isTutorialBot;

			this.avatar = data.avatar;
			this.shirt = data.shirt;

			//this.general = EvironGame.Objects.storage.GeneralInfo[data.general.artikul_id];
			if(EvironGame.Params.tutorial && this.isTutorialBot) this.hp = 20;
		},
		heal: function (data) {
			if(this.hp < 30 /*&& EvironGame.GRAPHIC == 1*/){
				EvironGame.Objects.Effects(EvironGame.card_43);
				EvironGame.Sounds.s_card_43.play();
			}
			this.hp = parseInt(data.hp);
			this.zones.HP.update();
			EvironGame.Objects.EventHandlerContinue();
		},
		setHP: function (x) {
			this.hp = parseInt(x);
			this.zones.HP.update();
		},
		useMana: function (x) {
			this.mana -= parseInt(x);
			this.zones.Mana.update();
			EvironGame.Objects.CanPlay();
			EvironGame.Objects.EventHandlerContinue();
		},
		setMana: function (x, effect) {
			this.mana = parseInt(x);
			this.zones.Mana.update();
			if(effect == 'EFFECT' /*&& EvironGame.GRAPHIC == 1*/){ EvironGame.Objects.Effects(EvironGame.card_66); EvironGame.Sounds.s_card_66.play(); }
			EvironGame.Objects.EventHandlerContinue();
		},
		setMaxMana: function (x) {
			this.MaxMana = parseInt(x);
			this.zones.Mana.update();
			EvironGame.Objects.EventHandlerContinue();
		},
		increaseMaxMana: function (x, effect) {
			var oldMana = this.MaxMana;
			this.MaxMana = parseInt(x);
			this.zones.Mana.update();
			if(effect == 'EFFECT' && parseInt(x) > oldMana /*&& EvironGame.GRAPHIC == 1*/){ EvironGame.Objects.Effects(EvironGame.card_64); EvironGame.Sounds.s_card_64.play(); }
			if(effect == 'EFFECT' && parseInt(x) < oldMana /*&& EvironGame.GRAPHIC == 1*/){ EvironGame.Objects.Effects(EvironGame.card_65); EvironGame.Sounds.s_card_65.play(); }
			EvironGame.Objects.EventHandlerContinue();
		},
		draw: function (cardArr) {
			for (var i = 0; i < cardArr.length; i++) {
				var card = new EvironGame.Card(cardArr[i]);
				card.draw();
			}
			this.zones.Hand.sort();
		},
		setDeck: function (deck) {
		   this.deck = deck;
		   this.zones.Library.update();
		},
		setGeneralSummon: function (GeneralSummon) {
		   this.GeneralSummon = GeneralSummon;
		},
		setGeneralAbility: function (GeneralAbility) {
		   this.GeneralAbility = GeneralAbility;
		},
		discard: function (data) {
			var cards = [];

			for (var i = 0; i< data.length; i++) {
				cards.push(EvironGame.GetObject(data[i]));
			}

			for (var i = 0; i < cards.length; i++) {
				cards[i].toGrave();
				delete this.hand[cards[i].sorted];
			}
			this.zones.Hand.sort();
			EvironGame.Objects.EventHandlerContinue();
		},
		turn: function () {

			EvironGame.yturn.alpha=1;
			EvironGame.yturnTween = EvironGame.Scene.add.tween(EvironGame.yturn);
			EvironGame.Scene.time.events.add(Phaser.Timer.SECOND * 1, function(){
				EvironGame.yturnTween.to( { alpha: 0 }, 100, Phaser.Easing.Linear.None, true);
				EvironGame.TURN_TIME = 0;
			}, this);

			EvironGame.TURN_TIME = 2000;
			EvironGame.Objects.storage.myturn = true;
			EvironGame.Objects.storage.EndButton.changeStatus();
			this.battleReset();

			for (var i in this.battle) {
				var card = this.battle[i];
				card.info.attack.first++;
				if(!card.info.attack.sickness) card.sleepSet();			
			}
			
			EvironGame.Objects.TrgStop(this.opponent.battle);
			this.setGeneralAbility(false);
			EvironGame.Objects.LightTargets('CreatureAvailMask');
		},

		battleReset: function () {
			for (var i in this.battle) {
				this.battle[i].info.attack.count = 0;
				this.battle[i].info.attack.sickness = false;
			}
		},
		endTurn: function () {
			EvironGame.Objects.storage.myturn = false;
			EvironGame.Objects.storage.EndButton.changeStatus();

			if (EvironGame.Objects.storage.MarkAttackCard) {
				EvironGame.Objects.storage.MarkAttackCard.unmarkToAttack();
			}

			for (var i in this.opponent.battle) {
				var card = this.opponent.battle[i];
				card.info.attack.first++;
				if(card.info.attack.sickness) card.sleepSet();
			}

			EvironGame.Objects.TrgStop(this.battle);
			EvironGame.Objects.LightTargetsStop();
		},
		playCard: function (data) {
			EvironGame.Objects.storage.ShowPlayedCard.show(data);
			var object = this.hand[data.position];
			object.toGrave();
			delete this.hand[data.position];
			this.zones.Hand.sort();
			EvironGame.Objects.EventHandlerContinue();
		},
		playTrap: function (data) {
			this.trap.push(data);
			this.zones.Trap.update();
		},
		dealtDamage: function (damage, attaker_type) {
			this.hp -= parseInt(damage);
			this.zones.HP.update();

			this.zones.Avatar.dealtDamage(attaker_type);
		},
		RemoveTrap: function (card) {
			var traps = [];
			for (var i in this.trap) {
				if (i == card.position) continue;
				traps.push(this.trap[i]);
			}
			
			this.trap[card.position].image.children[0].alpha = 1;
			this.trap[card.position].image.children[0].events.onAnimationComplete.add(function(){ 
				this.trap = traps;
				this.zones.Trap.update();
			}, this);
			this.trap[card.position].image.children[0].animations.play('play');
			EvironGame.Sounds.s_card_48.play();
		}
	}
	
	EvironGame.Objects.ParseEvent = function (event) {

		var type = event.type;
		var data = event.data;

		var player = EvironGame.Objects.storage.Players[event.owner];
		var skipEventHandler = false;

		switch (type) {
			case 'StartData':

				EvironGame.Objects.StartData(data);
				
				EvironGame.Objects.storage.Player.setInfo(data.players.player);
				EvironGame.Objects.storage.Opponent.setInfo(data.players.opponent);

				EvironGame.Objects.storage.Players[EvironGame.Objects.storage.Player.id]   = EvironGame.Objects.storage.Player;
				EvironGame.Objects.storage.Players[EvironGame.Objects.storage.Opponent.id] = EvironGame.Objects.storage.Opponent;
				
				break;
			case 'Reload':
				location.reload();
				break;
			case 'EndTurn':
				if (EVIRON.Game.storage.Players[event.owner].id == EVIRON.Game.storage.Player.id)  EVIRON.Game.storage.Player.endTurn();
				break;
			case 'Turn':
				EVIRON.Game.storage.Player.zones.Battle.sort();
				if (EVIRON.Game.storage.Players[event.owner].id == EVIRON.Game.storage.Player.id) player.turn();
				break;
			case 'SelectTarget':
				EVIRON.Game.storage.SelectMode = true;
				EVIRON.Game.LightTargets(data.mask);
				break;
			case 'SelectTargetOff':
				EVIRON.Game.storage.SelectMode = false;
				EVIRON.Game.LightTargets('CreatureAvailMask');
				break;
			case 'SummonCreatureEffect': /*if(EVIRON.GRAPHIC == 1)*/ EVIRON.Game.Effects(EVIRON.card_51); EVIRON.Sounds.s_card_51.play();
				break;
			case 'SummonCreatureCard': 
			case 'SummonCreature':
				var card = new EVIRON.Card(data);
				card.toBattle();
				player.cards[card.id] = card;
				break;
			case 'PlayGeneral':
				player.zones.General.hide();
				break;
			case 'GeneralDie':
				player.zones.General.show();
				break;
			case 'ShowGeneralCard': EVIRON.Game.storage.ShowGeneralCard.show(data.player, data.general); break;
			case 'PlayGeneralCard':  EVIRON.Game.storage.ShowGeneralCard.show(data, true); break;
			case 'Game': /*EvironGame.game.state.start('GameStart');*/ break;
			case 'CopyCard': /*if(EVIRON.GRAPHIC == 1)*/ EVIRON.Game.Effects(EVIRON.card_52); EVIRON.Sounds.s_card_52.play(); player.deck++;
			case 'Draw': player.draw(data); skipEventHandler = true; EVIRON.Game.CanPlay(); break; 
			case 'Deck': player.setDeck(data); skipEventHandler = true; break;
			case 'Discard': player.discard(data); skipEventHandler = true; break;
			case 'RedMaxMana': 
			case 'IncMaxMana': player.increaseMaxMana(event.data.val, event.data.effect); skipEventHandler = true; break;
			case 'SetMana': player.setMana(event.data.val, event.data.effect); skipEventHandler = true; break;
			case 'UseMana': player.useMana(data); skipEventHandler = true; break;
			case 'DealtDamage': EVIRON.GetObject(data.target).dealtDamage(data.damage, data.attaker_type); skipEventHandler = true; break;
			case 'cancelCard': /*if(EVIRON.GRAPHIC == 1)*/ EVIRON.Game.Effects(EVIRON.card_47); EVIRON.Sounds.s_card_47.play(); break;
			case 'cancelCardTurn': if(EVIRON.Game.storage.myturn){ EVIRON.Game.Effects(EVIRON.card_47); EVIRON.Sounds.s_card_47.play(); } player.deck--; player.zones.Library.update(); break;
			case 'PlayCard': player.playCard(data); skipEventHandler = true; EVIRON.Game.CanPlay(); break;
			case 'PlayTrap': player.playTrap(data); skipEventHandler = true; break;
			case 'Attack': EVIRON.GetObject(data.attacker).attack(data); skipEventHandler = true; EVIRON.Game.CanPlay(); break;
			case 'CreatureDie': EVIRON.GetObject(data.target).die(); skipEventHandler = true; break;
			case 'Defend':   EVIRON.GetObject(data.target).defend(); skipEventHandler = true; break;
			case 'Spell':   EVIRON.GetObject(data.target).spellEffect(data.animation); break;
			case 'Undefend': EVIRON.GetObject(data.target).undefend(); skipEventHandler = true; break;
			case 'Debuff':case 'SetPower':case 'SetToughness':
			case 'Buff': if(EVIRON.GetObject(data.target)){ EVIRON.GetObject(data.target).buff(data.target); skipEventHandler = true;} break
			case 'Heal': EVIRON.GetObject(data.target).heal(data.target); skipEventHandler = true; break;
			case 'FinishGame': EVIRON.Game.finish(data); skipEventHandler = true; break;
			case 'Reconnect': EVIRON.Game.reconnect(data); skipEventHandler = true; break;
			case 'GainAbility':
				EVIRON.GetObject(data.target).changeAbility(data.target.ability);
				skipEventHandler = true;
				break;
			case 'LoseAbility':
				EVIRON.GetObject(data.target).changeAbility(data.target.ability);
				skipEventHandler = true;
				break;
			case 'Bounce':
				EVIRON.GetObject(data.target).toGrave();
				delete player.battle[EVIRON.GetObject(data.target).sorted];
				player.zones.Battle.sort();
				/*if(EVIRON.GRAPHIC == 1)*/ 
				EVIRON.Game.Effects(EVIRON.card_67); 
				EVIRON.Sounds.s_card_67.play(); 
				if(EVIRON.GetObject(data.target).cfg.type == EVIRON.CARD_TYPE_GENERAL){
					player.zones.General.show();
				}else{
					cardArray = [];
					cardArray.push(data.target);
					player.draw(cardArray);				
				}
				skipEventHandler = true;
				break;
			case 'ChangeController':
				EVIRON.GetObject(data.target).ChangeController();
				skipEventHandler = true;
				break;
			case 'ChangeCost':
				EVIRON.GetObject(data.target).ChangeCost(data.target);
				skipEventHandler = true;
				break     
			case 'ChangeTrg':
				EVIRON.GetObject(data.target).ChangeTrg(data.target);
				skipEventHandler = true;
				break
			case 'ShuffleIn':
				/*if(EVIRON.GRAPHIC == 1)*/ EVIRON.GetObject(data.target).shuffleSet();
				break;
			case 'CreatureStun':
				EVIRON.GetObject(data.target).stun(data.stun);
				break;
			case 'ShowTrap':
				EVIRON.Game.storage.ShowPlayedCard.show(data);
				EVIRON.Game.storage.Players[data.owner].RemoveTrap(data);
				break;
			case 'RemoveTraps':
				EVIRON.Game.storage.Players[data.owner].RemoveTrap(data);
				break;
			case 'Pause':
				EVIRON.Game.Tutorial(data.key, data.id);
				break;
			case 'oponentDisconnect':
				EVIRON.Game.waiting(data);
				break;
			case 'oponentConnectBack':
				EVIRON.graphics.inputEnabled = false;
				EVIRON.graphics.alpha = 0;
				EVIRON.Waiting.removeAll();
				break;
		}


		if (!skipEventHandler) EvironGame.Objects.EventHandlerContinue();
	}
	
	EvironGame.Objects.StartData = function (data) {
		if(data.tutorial && location.href.indexOf('tutorial=') === -1) EvironGame.Params.tutorial = true;
		//EVIRON.Game.storage.GeneralInfo = data.generalInfo;
		
		//EVIRON.Game.storage.GeneralInfo = data.generalInfo;
		EvironGame.game.load.image('table', EvironGame.path.boards + data.board);
		EvironGame.game.load.image('avatarPlayer', EvironGame.path.avatars + data.players.player.avatar);
		EvironGame.game.load.image('avatarOpponent', EvironGame.path.avatars + data.players.opponent.avatar);
		EvironGame.game.load.image('shirtPlayer', EvironGame.path.shirts + data.players.player.shirt);
		EvironGame.game.load.image('shirtOpponent', EvironGame.path.shirts + data.players.opponent.shirt);
		EvironGame.game.load.image('generalPlayer', EvironGame.path.generals + EvironGame.generals[data.players.player.general.artikul_id].img);
		EvironGame.game.load.image('generalOpponent', EvironGame.path.generals + EvironGame.generals[data.players.opponent.general.artikul_id].img);
		/*EvironGame.game.load.image('generalTrapPlayer', EVIRON.PATH_CARD_IMG + 'trap_' + data.generalInfo[data.players.player.general.artikul_id].img);
		EvironGame.game.load.image('generalTrapOpponent', EVIRON.PATH_CARD_IMG + 'trap_' + data.generalInfo[data.players.opponent.general.artikul_id].img);*/
		/*EvironGame.game.load.image('generalPlayerSummonButton', EVIRON.PATH_GENERAL_ABILITY + 'general-button-' + EVIRON.CARD_ELEMENT_HASH[data.generalInfo[data.players.player.general.artikul_id].element] + '2.png');
		EvironGame.game.load.image('generalPlayerAbilityButton', EVIRON.PATH_GENERAL_ABILITY + 'general-button-' + EVIRON.CARD_ELEMENT_HASH[data.generalInfo[data.players.player.general.artikul_id].element] + '1.png');
		EvironGame.game.load.image('generalOpponentSummonButton', EVIRON.PATH_GENERAL_ABILITY + 'general-button-' + EVIRON.CARD_ELEMENT_HASH[data.generalInfo[data.players.opponent.general.artikul_id].element] + '2.png');
		EvironGame.game.load.image('generalOpponentAbilityButton', EVIRON.PATH_GENERAL_ABILITY + 'general-button-' + EVIRON.CARD_ELEMENT_HASH[data.generalInfo[data.players.opponent.general.artikul_id].element] + '1.png');*/
		EvironGame.game.load.onLoadComplete.addOnce( function() {  EvironGame.game.state.start('start'); } );
		/*if(EVIRON.Params.tutorial){
			EvironGame.game.load.image('dialog0',  EVIRON.PATH_TUTORIAL + 'dialog0.png');
			EvironGame.game.load.image('dialog1',  EVIRON.PATH_TUTORIAL + 'dialog1.png');
			EvironGame.game.load.image('dialog2',  EVIRON.PATH_TUTORIAL + 'dialog2.png');
			EvironGame.game.load.image('btnTurn',  EVIRON.PATH_TUTORIAL + 'btnTurn.png');
			EvironGame.game.load.image('arrow',  EVIRON.PATH_TUTORIAL + 'arrow.png');
		}*/
		EvironGame.game.load.start();
	}
	
EvironGame.Objects.PlayerZones = function () {

	var player = EvironGame.Objects.storage.Player;
    var opponent = EvironGame.Objects.storage.Opponent;

    var zones = [/* 'Mana', 'HP',*/ 'Avatar'/*, 'General', 'GeneralAbility', 'GeneralSummon', 'Library', 'Shirt', 'Nick','Battle', 'Trap', 'Hand'*/ ];

    var players = [ player, opponent ];

    var zoneKey, zone, i, j;

    for (j in players) {
        for (i in zones) {
            zoneKey = zones[i];
            zone = new EvironGame.Objects.Zone[zoneKey]();

            players[j].zones[zoneKey] = zone;

            if (j != 1) { zone.bindToPlayer(); } else { zone.bindToOpponent(); }
        }
    }
}

EvironGame.Objects.Zone = {};

EvironGame.Objects.Zone.Avatar = function () {

}

EvironGame.Objects.Zone.Avatar.prototype = {

    constructor: EvironGame.Objects.Zone.Avatar,
    bindToPlayer: function () {
        this.avatar = EvironGame.game.add.image(EvironGame.game.world.centerX-1, EvironGame.game.world.height-227, 'avatarPlayer');
		this.avatar.scale.setTo(0.52, 0.52);
		this.avatar.anchor.setTo(0.5, 0.5);
		this.frame = EvironGame.game.add.image(EvironGame.game.world.centerX-120, EvironGame.game.world.height-347, 'framePlayer');
		this.animPosition = {x:this.frame.left+(this.frame.width/2),y:this.frame.top+(this.frame.height/2)+5};
		this.owner = true;
		//this.update();
    },
    bindToOpponent: function () {
		this.avatar = EvironGame.game.add.image(EvironGame.game.world.centerX-1, 332, 'avatarOpponent');
		this.avatar.scale.setTo(0.52, 0.52);
		this.avatar.anchor.setTo(0.5, 1);
		this.frame = EvironGame.game.add.image(EvironGame.game.world.centerX-120, 115, 'frameOpponent');
		this.animPosition = {x:this.frame.left+(this.frame.width/2),y:this.frame.top+(this.frame.height/2)-5};
		this.owner = false;
		//this.update();
    },
	update: function () {
		this.avatar.inputEnabled = true;
		this.avatar.input.useHandCursor = true;
		this.avatar.events.onInputUp.add(this.click, this);
	
		this.card_18 = EvironGame.game.sprite(this.animPosition.x, this.animPosition.y, 'card_18');
		this.card_18.animations.add('play', Phaser.Animation.generateFrameNames('Card_18_', 0, 32, '', 4), 30, true);
		this.card_18.anchor.setTo(0.5, 0.5);
		this.card_18.alpha = 0;

		this.card_2_2 = EvironGame.game.add.sprite(this.animPosition.x, this.animPosition.y, 'card_2_2');
		this.card_2_2.animations.add('play', Phaser.Animation.generateFrameNames('Card_2_2_', 0, 22, '', 4), 30, false);
		this.card_2_2.anchor.setTo(0.5, 0.5);
		this.card_2_2.alpha = 0;
	
		if(this.owner){var y = this.animPosition.y+1;}else{var y = this.animPosition.y-1;}
		this.card_22 = EvironGame.game.add.sprite(this.animPosition.x, y, 'card_22');
		this.card_22.animations.add('play', Phaser.Animation.generateFrameNames('Card_22_', 0, 77, '', 4), 30, false);
		this.card_22.anchor.setTo(0.5, 0.5);
		this.card_22.alpha = 0;

		this.card_23 = EvironGame.game.add.sprite(this.animPosition.x, y, 'card_23');
		this.card_23.animations.add('play', Phaser.Animation.generateFrameNames('Card_23_', 0, 101, '', 4), 30, false);
		this.card_23.anchor.setTo(0.5, 0.5);
		this.card_23.alpha = 0;

		this.card_24 = EvironGame.game.add.sprite(this.animPosition.x, y, 'card_24');
		this.card_24.animations.add('play', Phaser.Animation.generateFrameNames('Card_24_', 0, 103, '', 4), 30, false);
		this.card_24.anchor.setTo(0.5, 0.5);
		this.card_24.alpha = 0;

		this.card_25 = EvironGame.game.add.sprite(this.animPosition.x, y, 'card_25');
		this.card_25.animations.add('play', Phaser.Animation.generateFrameNames('Card_25_', 0, 84, '', 4), 30, false);
		this.card_25.anchor.setTo(0.5, 0.5);
		this.card_25.alpha = 0;

		this.card_26 = EvironGame.game.add.sprite(this.animPosition.x, y, 'card_26');
		this.card_26.animations.add('play', Phaser.Animation.generateFrameNames('Card_26_', 0, 57, '', 4), 30, false);
		this.card_26.anchor.setTo(0.5, 0.5);
		this.card_26.alpha = 0;

		this.card_27 = EvironGame.game.add.sprite(this.animPosition.x, y, 'card_27');
		this.card_27.animations.add('play', Phaser.Animation.generateFrameNames('Card_27_', 0, 57, '', 4), 30, false);
		this.card_27.anchor.setTo(0.5, 0.5);
		this.card_27.alpha = 0;
	},
    light: function () {
        this.card_18.alpha = 1;
		this.card_18.animations.play('play');	
    },
    lightStop: function () {
		this.card_18.alpha = 0;
		this.card_18.animations.stop('play');
    }/*,
    dealtDamage: function (artikul) {
		var animation;
		
		if(artikul){
			switch (artikul.element) {
				case EVIRON.CARD_ELEMENT_GROUND: animation = this.card_22; this.sound = EVIRON.Sounds.s_card_28; break;
				case EVIRON.CARD_ELEMENT_FIRE: animation = this.card_23; this.sound = EVIRON.Sounds.s_card_29; break;
				case EVIRON.CARD_ELEMENT_WATER: animation = this.card_24; this.sound = EVIRON.Sounds.s_card_30; break;
				case EVIRON.CARD_ELEMENT_DEATH: animation = this.card_25; this.sound = EVIRON.Sounds.s_card_31; break;
				case EVIRON.CARD_ELEMENT_AIR: animation = this.card_26; this.sound = EVIRON.Sounds.s_card_32; break;
				case EVIRON.CARD_ELEMENT_LIFE: animation = this.card_27; this.sound = EVIRON.Sounds.s_card_33; break;
				default: animation = this.card_2_2; this.sound = EVIRON.Sounds.s_card_2_2; break;
			}
		}else{
			animation = this.card_2_2;
			this.sound = EVIRON.Sounds.s_card_2_2;
		}
		animation.alpha = 1;
		animation.animations.play('play');	
		animation.events.onAnimationComplete.add(function(){ animation.alpha = 0; EVIRON.Game.EventHandlerContinue(); }, this);
		this.sound.play();
		
	},
	getOwner: function () {
        return EVIRON.Game.storage[this.owner ? 'Player' : 'Opponent'];
    },
	click: function (){
		if (EVIRON.Game.storage.MarkAttackCard || EVIRON.Game.storage.SelectMode) {
			if (EVIRON.Game.storage.SelectMode) {
				var player = this.getOwner();
				if (EVIRON.Game.storage.UnresolvedCard) {
					EVIRON.Game.storage.SelectModeTarget = player;
					EVIRON.Game.storage.UnresolvedCard.click();
				} else {

					EVIRON.Emit('SelectTargetConfirm', {target: player.id, type: 'player'});
				}
			} else {
				EVIRON.Emit('Attack', {attacker: EVIRON.Game.storage.MarkAttackCard.id, player: true});
			}
		}
	}*/
}

EvironGame.Objects.Zone.Nick = function () {
	this.textStyle = {font: "25px Arial", fill: '#ffffff', align: "center"};
}
EvironGame.Objects.Zone.Nick.prototype = {
    constructor: EvironGame.Objects.Zone.Nick,
    bindToPlayer: function () {
		this.nick = EvironGame.game.add.text(1233, 753, EvironGame.Objects.storage.Player.nick, this.textStyle);
		this.nick.alpha = 0.7;
		this.nick.anchor.setTo(1, 0);
		this.nick.inputEnabled = true;
		this.nick.input.useHandCursor = true;
		this.owner = true;
		this.nick.events.onInputOver.add(this.hover, this );
		this.nick.events.onInputOut.add(this.standart, this );
		this.nick.events.onInputUp.add(this.click, this );
    },
    bindToOpponent: function () {
      	this.nick = EvironGame.game.add.text(200, 292, EvironGame.Objects.storage.Opponent.nick, this.textStyle);
		this.nick.alpha = 0.7;
		this.nick.inputEnabled = true;
		this.nick.input.useHandCursor = true;
		this.owner = false;
		EvironGame.game.nick_buttons = false;
		this.nick.events.onInputOver.add(this.hover, this );
		this.nick.events.onInputOut.add(this.standart, this );
		this.nick.events.onInputUp.add(this.click, this );
    },
	hover: function (){
		this.nick.alpha = 1;
	},
	standart: function (){
		this.nick.alpha = 0.7;
	},
	click: function (){
		if(this.owner){
			window.open('/profile/' + EVIRON.Game.storage.Player.id, '_blank');
		}else{
			if(!EVIRON.Scene.nick_buttons){
				EVIRON.Scene.nick_buttons = EVIRON.Scene.add.group();
				var buttonFriend = EVIRON.Scene.add.button(this.nick.left, this.nick.bottom, 'buttonSmall', this.addToFriend, this, 'over', 'out');
				buttonFriend.input.priorityID = 1;
				var textFriend = EVIRON.Scene.add.text(buttonFriend.left+(buttonFriend.width/2), buttonFriend.top+(buttonFriend.height/2)+2, "В друзья",  { font: "18px Arial", fill: '#ffffff', align: "center"});
				textFriend.anchor.setTo(0.5, 0.5);
				var buttonProfile = EVIRON.Scene.add.button(buttonFriend.left, buttonFriend.bottom+5, 'buttonSmall', function(){ if(EVIRON.Game.storage.Opponent.isBot){ EVIRON.Game.Info('Я не игрок - у меня, к сожалению, нет профиля!', 'robot', 0); }else{window.open('/profile/' + EVIRON.Game.storage.Opponent.id, '_blank'); }}, this, 'over', 'out');
				buttonProfile.input.priorityID = 1;
				var textProfile = EVIRON.Scene.add.text(buttonProfile.left+(buttonProfile.width/2), buttonProfile.top+(buttonProfile.height/2)+2, "Профиль",  { font: "18px Arial", fill: '#ffffff', align: "center"});
				textProfile.anchor.setTo(0.5, 0.5);
				
				EVIRON.Scene.nick_buttons.add(buttonFriend);
				EVIRON.Scene.nick_buttons.add(textFriend);
				EVIRON.Scene.nick_buttons.add(buttonProfile);
				EVIRON.Scene.nick_buttons.add(textProfile);
			}else{
				EVIRON.Scene.nick_buttons.destroy();
				EVIRON.Scene.nick_buttons = false;
			}
		}
	},
	addToFriend: function (){
		if(EVIRON.Game.storage.Opponent.isBot){
			EVIRON.Game.Info('Вы не можете добавить меня в друзья, поскольку я не игрок!', 'robot', 0);
		}else{
			$.ajax({
				url: '/profile/' +EVIRON.Game.storage.Opponent.id+ '/addFriends',
				dataType: 'json',
				success: function (e) {
					if (e.status != DATA_OK) {
						EVIRON.Game.Info(e.err, 'swallow', -40);
					} else {
						if(e.add_success == 1){
							EVIRON.Game.Info('Теперь вы друзья', 'swallow', -40);
						} else {
							EVIRON.Game.Info('Заявка отправлена', 'swallow', -40);
						}
					}
				}
			});
		}
	}
}