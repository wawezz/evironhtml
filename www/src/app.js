(function () {
	var engine;
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		engine = Phaser.AUTO;
	}else{
		engine = Phaser.CANVAS;
	}
	
    EvironGame.game = new Phaser.Game(EvironGame.table.width, EvironGame.table.height, engine, 'game');
    EvironGame.game.state.add('boot', EvironGame.boot);
    EvironGame.game.state.add('preload', EvironGame.preload);
    EvironGame.game.state.add('start', EvironGame.start);

    EvironGame.game.state.start('boot');
})();